export class Util {
    static convertISODateToYYMMDDDD(IsoDate: string): Date {
        const date = new Date(IsoDate);
        return new Date(date.getFullYear(), date.getMonth(), date.getDate());
    }
}