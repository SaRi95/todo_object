import { Injectable } from '@angular/core';
import { Task } from 'src/task';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private items: Array<Task> = [];

  constructor() { }

  public getItems(): Array<Task> {
    return this.items;
  }

  public getItemCount(): number {
    return this.items.length;
  }

  public addItem(task: Task) {
    this.items.push(task);
  }

  public removeItem(index: number) {
    this.items.splice(index, 1);
  }
}
